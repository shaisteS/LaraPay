<?php


namespace App\Repositories\Contracts;


interface GatewayTransactionRepositoryInterface extends RepositoryInterface
{
    public function updateStatus(int $id,int $status);
}