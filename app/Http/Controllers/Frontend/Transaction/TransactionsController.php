<?php

namespace App\Http\Controllers\Frontend\Transaction;

use App\Repositories\Contracts\BankTransactionRepositoryInterface;
use App\Repositories\Contracts\GatewayTransactionRepositoryInterface;
use App\Repositories\Eloquent\Transaction\BankTransactionStatus;
use App\Services\GatewayTransaction\GatewayTransactionService;
use App\Services\GatewayTransaction\TransactionRequest;
use App\Services\Payment\PaymentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionsController extends Controller
{
    /**
     * @var GatewayTransactionService
     */
    private $gatewayTransactionService;
    /**
     * @var GatewayTransactionRepositoryInterface
     */
    private $gatewayTransactionRepository;

    public function __construct(
        GatewayTransactionService $gatewayTransactionService,
        GatewayTransactionRepositoryInterface $gatewayTransactionRepository
    ) {

        $this->gatewayTransactionService = $gatewayTransactionService;
        $this->gatewayTransactionRepository = $gatewayTransactionRepository;
    }

    public function request(Request $request)
    {
        try {
            $transactionKey = $this->gatewayTransactionService->make(new TransactionRequest([
                'token' => $request->token,
                'amount' => $request->amount,
                'res_number' => $request->res_number,
                'ip' => $request->ip(),
                'domain' => $request->getHost(),
                "callback" => $request->callback,
                'description' => ""
            ]));
            return response()->json([
                'success' => true,
                'transactionKey' => $transactionKey
            ]);
        } catch (\Exception $exception) {

            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ]);

        }

    }

    public function start(Request $request)
    {
        $transactionKey = $request->transactionKey;
        $transaction = $this->gatewayTransactionRepository->findBy([
            'gateway_transaction_key' => $transactionKey
        ]);
        if (is_null($transaction)) {
            abort(404);
        }
        $bankTransactionRepository = resolve(BankTransactionRepositoryInterface::class);
        $newBankTransaction = $bankTransactionRepository->store([
            'bank_transaction_bank_id' => 1,
            'bank_transaction_gateway_transaction_id' => $transaction->gateway_transaction_id,
            'bank_transaction_res_number' => $this->getResNum(),
            'bank_transaction_amount' => $transaction->gateway_transaction_amount,
            'bank_transaction_status' => BankTransactionStatus::PENDING
        ]);
        if ($newBankTransaction) {
            $paymentService = new PaymentService();
            return $paymentService->doPayment($newBankTransaction->bank_transaction_id);
        }

    }

    public function verify(Request $request)
    {
        $this->gatewayTransactionRepository->beginTransaction();
        try {
            $verifyResult = $this->gatewayTransactionService->verify([
                'token' => $request->token,
                'transactionKey' => $request->transactionKey,
                'amount' => $request->amount,
                'res_number' => $request->res_number
            ]);
            $this->gatewayTransactionRepository->commit();
            return response()->json($verifyResult);

        } catch (\Exception $exception) {
            $this->gatewayTransactionRepository->rollBack();
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }

    }

    private function getResNum()
    {
        return str_replace('.', '', microtime(true)) . mt_rand(9999, 99999);

    }
}
