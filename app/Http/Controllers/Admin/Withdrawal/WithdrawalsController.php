<?php

namespace App\Http\Controllers\Admin\Withdrawal;

use App\Models\Withdrawal;
use App\Repositories\Contracts\GatewayRepositoryInterface;
use App\Repositories\Contracts\WithdrawalRepositoryInterface;
use App\Services\Withdrawal\Validator\TransactionValidator;
use App\Services\Withdrawal\WithdrawalRequest;
use App\Services\Withdrawal\WithdrawalService;
use App\ValueObjects\Mobile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Monolog\Handler\SlackWebhookHandler;

class WithdrawalsController extends Controller
{
    private $withdrawal_service;
    /**
     * @var WithdrawalRepositoryInterface
     */
    private $withdrawalRepository;

    public function __construct(WithdrawalRepositoryInterface $withdrawalRepository)
    {
        $this->withdrawal_service   = new WithdrawalService();
        $this->withdrawalRepository = $withdrawalRepository;
    }

    public function index()
    {
        $withdrawals = $this->withdrawalRepository->all(null, ['gateway', 'account.owner']);

        return view('admin.withdrawal.index', compact('withdrawals'));
    }

    public function create()
    {
        $statuses       = Withdrawal::getStatuses();
        $withdrawalItem = null;

        return view('admin.withdrawal.create', compact('statuses', 'withdrawalItem'));
    }

    public function store(Request $request)
    {
        $gateway_repository = resolve(GatewayRepositoryInterface::class);
        $gateway_item       = $gateway_repository->findWith($request->gateway, ['plan']);
        $gateway_item       = $gateway_item->first();
        $withdrawalRequest  = WithdrawalRequest::fromArray([
            'gateway'    => $request->gateway,
            'account'    => $request->account,
            'amount'     => $request->amount,
            'commission' => $gateway_item->plan->gateway_plan_commission,
            'rate'       => $gateway_item->plan->gateway_plan_withdrawal_rate,
            'max'        => $gateway_item->plan->gateway_plan_withdrawal_max,
            'status'     => $request->status
        ]);
        try {
            $result = $this->withdrawal_service->create($withdrawalRequest);
            if ( ! $result) {
                return back()->withErrors([
                    'error' => 'ثبت درخواست با خطا مواجه شد، لطفا بعدا امتحان کنید'
                ]);
            }

            return redirect()->route('admin.withdrawals.index')->with('success', 'درخواست با موفقیت ثبت شد!');
        } catch (\Exception $exception) {
            //$handler = new SlackWebhookHandler()
            return back()->withErrors([
                'error' => $exception->getMessage()
            ]);
        }

    }

    public function searchGateways(Request $request)
    {

    }

    public function approve(Request $request, $id)
    {
        $withdrawalItem = $this->getById($id);

        return view('admin.withdrawal.approve', compact('withdrawalItem'));

    }

    public function performApprove(Request $request, $id)
    {
//        $withdrawalItem = $this->getById($id);
        $approveResult = $this->withdrawal_service->approve($id, $request->ref_number);
        if ($approveResult) {
            return back()->with('success', 'درخواست با موفقیت تایید شد!');
        }

        return back()->withErrors([
            'error' => 'خطایی درتایید درخواست ایجاد شده است!بعدا امتحان کنید'
        ]);

    }

    public function reject(Request $request, $id)
    {
        $rejectStatus = $this->withdrawal_service->reject($id);
        if ($rejectStatus) {
            return back()->with('success', 'درخواست با موفقیت رد شد!');
        }

        return back()->withErrors([
            'error' => 'رد درخواست نا موفق بود، لطفا بعدا امتحان کنید!'
        ]);
    }

    private function getById($id)
    {
        $withdrawalItem = $this->withdrawalRepository->find($id);
        if ( ! $withdrawalItem) {
            abort(404);
        }

        return $withdrawalItem;
    }
}
