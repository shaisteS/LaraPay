<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Repositories\Contracts\BankTransactionRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankTransactionsController extends Controller
{
    /**
     * @var BankTransactionRepositoryInterface
     */
    private $bank_transaction_repository;

    public function __construct(BankTransactionRepositoryInterface $bank_transaction_repository)
    {

        $this->bank_transaction_repository = $bank_transaction_repository;
    }

    public function index()
    {
        $transactions = $this->bank_transaction_repository->all();

        return view('admin.transaction.bank.index', compact('transactions'));
    }
}
