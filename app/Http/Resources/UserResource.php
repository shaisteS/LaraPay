<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'user_id' => $this->id,
            'user_display_name' => $this->name,
            'status' => $this->present()->status,
            'accounts' => UserAccountResource::collection($this->accounts)
        ];
    }
}
