<?php


namespace App\Services\Notification\Providers;


use App\Services\Notification\Contracts\SendMethod;

class SmsProvider implements SendMethod
{

    public function send(array $args)
    {
        $gateway = $this->getDefaultGateway();
        $gateway->send();
    }

    private function getDefaultGateway()
    {
        $smsGateway = config('sms.driver');
        $smsGatewayClass = 'App\\Services\\Notification\\Providers\\Sms\\Gateways\\'.ucfirst($smsGateway);
        return new $smsGatewayClass;
    }
}