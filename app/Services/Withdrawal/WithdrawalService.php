<?php


namespace App\Services\Withdrawal;


use App\Events\Withdrawal\WithdrawalApproved;
use App\Models\Withdrawal;
use App\Repositories\Contracts\WithdrawalRepositoryInterface;
use App\Services\Gateway\AggregationService;
use App\Services\Withdrawal\Validator\WithdrawalValidator;
use Illuminate\Support\Facades\DB;

class WithdrawalService
{

    private $withdrawal_repository;

    public function __construct()
    {
        $this->withdrawal_repository = resolve(WithdrawalRepositoryInterface::class);
    }

    public function create(WithdrawalRequest $withdrawalRequest)
    {
        $withdrawalRequestValidator = new WithdrawalValidator();
        $withdrawalRequestValidator->validate($withdrawalRequest);

        $new_withdrawal = $this->withdrawal_repository->store([
            'withdrawal_gateway_id' => $withdrawalRequest->getGateway(),
            'withdrawal_user_account_id' => $withdrawalRequest->getAccount(),
            'withdrawal_amount' => $withdrawalRequest->getAmount(),
            'withdrawal_commission' => $withdrawalRequest->getCommission(),
            'withdrawal_status' => $withdrawalRequest->getStatus()
        ]);

        // TODO use Notification Service
        return !is_null($new_withdrawal);

    }

    public function approve(int $withdrawal_id, string $ref_code)
    {
        $this->withdrawal_repository->beginTransaction();
        $result = false;
        $aggregationService = new AggregationService();
        try {
            $withdrawalItem = $this->withdrawal_repository->find($withdrawal_id);
            $gateway = $withdrawalItem->gateway;
//            $user           = $withdrawalItem->account->owner;
            $gateway->decrement('gateway_balance', $withdrawalItem->withdrawal_amount);
            $updateResult = $withdrawalItem->update([
                'withdrawal_ref_number' => $ref_code,
                'withdrawal_status' => Withdrawal::DONE
            ]);
            if ($updateResult) {
                $aggregationService->withdrawal($withdrawalItem->withdrawal_gateway_id,
                    $withdrawalItem->withdrawal_amount);
            }
            // update report
            $this->withdrawal_repository->commit();
            $result = true;
            event(new WithdrawalApproved($withdrawalItem));
        } catch (\Exception $exception) {
            $this->withdrawal_repository->rollBack();
        }

        return $result;
    }

    public function reject(int $withdrawal_id)
    {
        $withdrawalItem = $this->withdrawal_repository->find($withdrawal_id);
        return $withdrawalItem->update([
            'withdrawal_status' => Withdrawal::REJECTED
        ]);
    }

}