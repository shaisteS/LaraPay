<?php

namespace App\Services\Reporter;


use App\Repositories\Contracts\PaymentRepositoryInterface;
use App\Services\Reporter\Output\ReporterOutput;
use Illuminate\Support\Facades\Auth;

class PaymentReporter
{
    private $repository;

    public function __construct()
    {
        $this->repository = resolve(PaymentRepositoryInterface::class);
    }

    public function between($startDate, $endDate,ReporterOutput $output)
    {
        $result = $this->getData($startDate, $endDate);
        return $output->output($result);
    }

    private function getData($startDate, $endDate)
    {
        return $this->repository->getReportData($startDate,$endDate);
    }


}