<?php


namespace App\Services\Reporter\Output;


class HtmlOutput implements ReporterOutput
{

    public function output($data)
    {
        return "<h1>$data</h1>";
    }
}