<?php


namespace App\Services\GatewayTransaction;


class TransactionVerifyRequest
{
    private $token;

    private $transactionKey;

    private $amount;

    private $resNumber;

    public function __construct(array $data)
    {
        $this->token = $data['token'];
        $this->transactionKey = $data['transactionKey'];
        $this->amount = $data['amount'];
        $this->resNumber = $data['res_number'];
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getTransactionKey()
    {
        return $this->transactionKey;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getResNumber()
    {
        return $this->resNumber;
    }
}