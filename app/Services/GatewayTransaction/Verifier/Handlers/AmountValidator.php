<?php


namespace App\Services\GatewayTransaction\Verifier\Handlers;


use App\Repositories\Contracts\GatewayTransactionRepositoryInterface;
use App\Services\GatewayTransaction\TransactionVerifyRequest;
use App\Services\GatewayTransaction\Validator\Exceptions\InvalidAmountException;
use App\Services\GatewayTransaction\Verifier\Contracts\Verifier;

class AmountValidator extends Verifier
{

    protected function process(TransactionVerifyRequest $request)
    {
        $gatewayTransactionRepository = resolve(GatewayTransactionRepositoryInterface::class);
        $transaction =$gatewayTransactionRepository->findBy([
            'gateway_transaction_key' => $request->getTransactionKey(),
            'gateway_transaction_amount' => $request->getAmount()
        ]);
        if(is_null($transaction))
        {
            throw new InvalidAmountException('invalid transaction or amount!');
        }
        return true;
    }
}