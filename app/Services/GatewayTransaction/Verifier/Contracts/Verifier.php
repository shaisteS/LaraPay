<?php


namespace App\Services\GatewayTransaction\Verifier\Contracts;


use App\Services\GatewayTransaction\TransactionVerifyRequest;

abstract class Verifier
{
    /**
     * @var Verifier
     */
    protected $nextValidator;

    public function __construct(Verifier $validator = null)
    {
        $this->nextValidator = $validator;
    }

    final public function handle(TransactionVerifyRequest $request)
    {
        $result = $this->process($request);
        if ($result) {
            if (!is_null($this->nextValidator)) {
                return $this->nextValidator->handle($request);
            }
            return $result;
        }
        return $result;

    }

    abstract protected function process(TransactionVerifyRequest $request);
}