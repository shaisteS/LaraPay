<?php


namespace App\Services\GatewayTransaction\Validator\Handlers;


use App\Repositories\Contracts\GatewayRepositoryInterface;
use App\Services\GatewayTransaction\TransactionRequest;
use App\Services\GatewayTransaction\Validator\Contracts\Validator;
use App\Services\GatewayTransaction\Validator\Exceptions\InvalidTokenException;

class TokenValidator extends Validator
{

    protected function process(TransactionRequest $request)
    {
        $gateway_repository = resolve(GatewayRepositoryInterface::class);
        $gateway = $gateway_repository->findBy([
            'gateway_access_token' => $request->getToken()
        ]);
        if(is_null($gateway))
        {
            throw new InvalidTokenException('invalid token!');
        }
        return true;
    }
}