<?php

namespace App\Events\Withdrawal;

use App\Models\Withdrawal;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WithdrawalApproved implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Withdrawal
     */
    public $withdrawal;

    /**
     * Create a new event instance.
     *
     * @param Withdrawal $withdrawal
     */
    public function __construct(Withdrawal $withdrawal)
    {
        //
        $this->withdrawal = $withdrawal;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('withdrawal.'.$this->withdrawal->withdrawal_id);
    }

    public function broadcastAs()
    {
        return 'approved';
    }

    public function broadcastWith()
    {
        return [
            'amount' => $this->withdrawal->present()->amount
        ];
    }
}
