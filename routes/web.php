<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    Route::get('/', 'DashboardController@index')->name('dashboard.index');
    Route::get('statistics', 'DashboardController@statistics')->name('dashboard.statistics');
    //admin users
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/', 'User\UsersController@index')->name('index');
        Route::get('add', 'User\UsersController@create')->name('create');
        Route::post('add', 'User\UsersController@store')->name('store');
        Route::get('accounts', 'User\UserAccountsController@index')->name('accounts.index');
        Route::get('accounts/add', 'User\UserAccountsController@create')->name('accounts.create');
        Route::post('accounts/add', 'User\UserAccountsController@store')->name('accounts.store');
        Route::get('accounts/remove/{id}', 'User\UserAccountsController@delete')->name('accounts.delete');
        Route::get('accounts/edit/{id}', 'User\UserAccountsController@edit')->name('accounts.edit');
        Route::post('accounts/edit/{id}', 'User\UserAccountsController@update')->name('accounts.update');
        Route::get('accounts/search', 'User\UserAccountsController@search')->name('accounts.search');
        Route::get('search', 'User\UsersController@search');
    });
    //admin gateways
    Route::group(['prefix' => 'gateways'], function () {
        Route::get('/', 'Gateway\GatewaysController@index')->name('gateways.index');
        Route::get('add', 'Gateway\GatewaysController@create')->name('gateways.create');
        Route::post('add', 'Gateway\GatewaysController@store')->name('gateways.store');
        Route::get('reports', 'Gateway\ReportsController@index')->name('gateways.reports.index');
        Route::get('search', 'Gateway\GatewaysController@search')->name('gateways.search');

        Route::group(['prefix' => 'plans'], function () {
            Route::get('/', 'Gateway\PlansController@index')->name('gateways.plans.index');
            Route::get('add', 'Gateway\PlansController@create')->name('gateways.plans.create');
            Route::post('add', 'Gateway\PlansController@store')->name('gateways.plans.store');
        });
    });
    //admin withdrawals
    Route::group(['prefix' => 'withdrawals'], function () {
        Route::get('/', 'Withdrawal\WithdrawalsController@index')->name('withdrawals.index');
        Route::get('add', 'Withdrawal\WithdrawalsController@create')->name('withdrawals.create');
        Route::post('add', 'Withdrawal\WithdrawalsController@store')->name('withdrawals.store');
        Route::get('approve/{id}', 'Withdrawal\WithdrawalsController@approve')->name('withdrawals.approve');
        Route::post('approve/{id}',
            'Withdrawal\WithdrawalsController@performApprove')->name('withdrawals.approve.save');
        Route::get('reject/{id}', 'Withdrawal\WithdrawalsController@reject')->name('withdrawals.reject');
    });
    //admin transactions
    Route::group(['prefix' => 'transactions'], function () {
        Route::get('gateway', 'Transaction\GatewayTransactionsController@index')->name('transactions.gateway.index');
        Route::get('bank', 'Transaction\BankTransactionsController@index')->name('transactions.bank.index');
    });
    //admin payments
    Route::group(['prefix' => 'payments'], function () {
        Route::get('/', 'Payment\PaymentsController@index')->name('payments.index');
    });
    //admin settings
    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', 'Setting\SettingsController@index')->name('settings.index');
        Route::get('create', 'Setting\SettingsController@create')->name('settings.create');
        Route::post('create', 'Setting\SettingsController@store')->name('settings.store');
    });

});
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    Route::get('pay/start', 'PaymentsController@start')->name('payment.start');
    Route::post('pay/verify/{payment_code}', 'PaymentsController@verify')->name('payment.verify');
    Route::post('transaction/request','Transaction\TransactionsController@request')->name('transaction.request');
    Route::get('transaction/start','Transaction\TransactionsController@start')->name('transaction.start');
    Route::post('transaction/verify','Transaction\TransactionsController@verify')->name('transaction.verify');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
